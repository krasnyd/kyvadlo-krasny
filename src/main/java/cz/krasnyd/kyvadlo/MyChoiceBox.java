package cz.krasnyd.kyvadlo;

import javafx.scene.control.ChoiceBox;

public class MyChoiceBox extends ChoiceBox<String>{
    private Jednotka selected = Jednotka.PERIODA;

    Jednotka getSelected() {
        return selected;
    }

    void setSelected(String value) {
        switch (value) {
            case "T":
                this.selected = Jednotka.PERIODA;
                break;
            case "f":
                this.selected = Jednotka.FREKVENCE;
                break;
            case "ω":
                this.selected = Jednotka.UH_FREKVENCE;
                break;
            default:
                System.out.println("Neznámá jednotka");
                break;
        }

    }
}
