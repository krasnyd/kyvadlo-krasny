package cz.krasnyd.kyvadlo;

public enum Jednotka {
    PERIODA,
    FREKVENCE,
    UH_FREKVENCE
}