package cz.krasnyd.kyvadlo;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ResourceBundle;
import java.util.function.Function;

public class MainAppController implements Initializable {

    @FXML
    private AreaChart<Double, Double> areaGraph;
    public NumberTextField ym, fi, cas, omega;
    public MyChoiceBox choice;
    public Label baseVzorecLabel, changedVzorecLabel, resultLabel;
    public Label omegaLabel;

    private MyGraph areaMathsGraph;

    @Override
    public void initialize(final URL url, final ResourceBundle rb) {
        areaMathsGraph = new MyGraph(areaGraph, 10);

        choice.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            choice.setSelected(newValue);
            if(choice.getSelected() == Jednotka.PERIODA){
                omegaLabel.setText("s");
            } else if(choice.getSelected() == Jednotka.FREKVENCE){
                omegaLabel.setText("Hz");
            } else if(choice.getSelected() == Jednotka.UH_FREKVENCE){
                omegaLabel.setText("s-1");
            }
        });

    }

    private void plotLine(Function<Double, Double> function) {
        areaMathsGraph.plotLine(function);
    }

    @FXML
    private void calc(final ActionEvent event) {
        Jednotka jednotka = choice.getSelected();
        double ymValue = ym.getValue();
        double fiValue = fi.getValue();
        double casValue = cas.getValue();
        double omegaValue = omega.getValue();

        String vzorec = "";

        // recalculate omega
        if(jednotka == Jednotka.FREKVENCE){
            omegaValue = 2 * Math.PI * omegaValue;
            vzorec = "2π * f";
        } else if(jednotka == Jednotka.PERIODA){
            omegaValue = (2 * Math.PI) / omegaValue;
            vzorec = "2π / T";
        } else if(jednotka == Jednotka.UH_FREKVENCE){
            vzorec = "ω";
        }


        double vysledek = ymValue * Math.sin(omegaValue * casValue + fiValue);
        String baseVzorec = "y = ym * sin("+vzorec+" * t + φ0)";
        String changedVzorec = "y = "+ymValue+" * sin("+formatDouble(omegaValue)+" * "+casValue+" + "+fiValue+")";
        String result = "y = "+formatDouble(vysledek);
        baseVzorecLabel.setText(baseVzorec);
        changedVzorecLabel.setText(changedVzorec);
        resultLabel.setText(result);

        double finalOmegaValue = omegaValue;
        clear();
        plotLine(x -> ymValue * Math.sin(finalOmegaValue * x + fiValue));
    }

    public static String formatDouble(double num){
        NumberFormat formatter = new DecimalFormat("#0.00");
        return formatter.format(num);
    }

    private void clear() {
        areaMathsGraph.clear();
    }
}