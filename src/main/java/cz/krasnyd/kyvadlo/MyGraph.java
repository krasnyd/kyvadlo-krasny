package cz.krasnyd.kyvadlo;

import javafx.scene.chart.XYChart;
import javafx.scene.shape.Rectangle;


import java.util.function.Function;

public class MyGraph {
    private XYChart<Double, Double> graph;
    private double range;

    public MyGraph(final XYChart<Double, Double> graph, final double range) {
        this.graph = graph;
        this.range = range;
    }

    public void plotLine(final Function<Double, Double> function) {
        final XYChart.Series<Double, Double> series = new XYChart.Series<Double, Double>();
        for (double x = -range; x <= range; x = x + 0.005) {
            plotPoint(x, function.apply(x), series);
        }
        graph.getData().add(series);

    }

    private void plotPoint(final double x, final double y,
                           final XYChart.Series<Double, Double> series) {
        XYChart.Data data = new XYChart.Data<Double, Double>(x, y);
        Rectangle rect = new Rectangle(0,0);
        rect.setVisible(false);
        data.setNode(rect);

        series.getData().add(data);
    }

    public void clear() {
        graph.getData().clear();
    }
}
